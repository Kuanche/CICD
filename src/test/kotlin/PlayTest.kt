import org.junit.Test
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowCallbackHandler
import org.springframework.jdbc.datasource.DriverManagerDataSource

class PlayTest {
    @Test
    fun `test database`() {
        val dataSource = DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver")
        dataSource.url = "jdbc:postgresql://postgres:5432/postgres"
        dataSource.username = "postgres"
        dataSource.password = "postgres"

        val jdbcTemplate = JdbcTemplate(dataSource);
        jdbcTemplate.execute("create table temp(id bigint primary key,name varchar(32))")
        jdbcTemplate.execute("insert into temp(id, name) values (1, 'james')")
        jdbcTemplate.query("select name from temp where id = 1", RowCallbackHandler {
            println(it.getString(1))
        })
    }

    @Test
    fun `test hello`() {
        print("hello world")
    }
}
